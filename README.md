# CSSyntax
Tento repozitář obsahuje makro pro zvýraznění syntaxe jazyka C# pro TeX (OPmac).
Toto makro vzniklo v rámci semestrální práce v předmětu Typografie a TeX.

Kompletní dokumentaci k tomuto makru naleznete v dokumentu [dokumentace.pdf](dokumentace.pdf).
Makro samotné se nachází v souboru [cssyntax.tex](cssyntax.tex).

## Zavedení makra

Celé makro lze vložit přímo do dokumentu.
Pokud nechcete vkládat makro přímo do dokumentu, můžete připojit soubor `cssyntax.tex`.
Toho docílíte pomocí příkazu `\input cssyntax`.

Po připojení makra lze před verbatim prostředí přidat `\hisyntax{CS}` nebo `\ghisyntax{CS}` pro lokální nebo globální nastavení obarvení.
Verbatim prostředí může ve zdrojovém textu vypadat například takto:
```
\hisyntax{CS}
\begtt
string foo = "bar";
...
\endtt
```